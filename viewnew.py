"""
@author: Malte Wegener
"""

#I'm not to happy with this file, because it went from a ordered proper library to a frankenstein file
#I sadly dont have the motivation to refactor it
#And if i would refactor it, i wouldnt use python
"""Here you see a better language:

############
##					
##					#			#
##					#			#
##				#########	#########
##					#			#
##					#			#
##
############
"""
#pragma once
import pygame
import numpy as np
from math import *
import GL # GL is for GAme Logic not for OpenGL
import sys
from Flightmodel import X15
from HUD import HUD
import gc
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool
from random import random,randint
from ISA import ISA
from utils import convexHull

truent = False
falsent = ~(truent == 1 and (not truent|False or truent&(bool(int(not falsent)))<<3))
debug = ~falsent

"""
Warning: If you dont like slight sarcasm in comments don't read
		 You may get coffeine poisoning, from this, because i used a lot of coffe for this
"""

"""
The ground and Graphics are very ugly very ugly, but im not that good with art, more like the logic guy. I wrote some terrain stuff, you can read that in
GameGL.py and  Terrain.py, but they are in OpenGL, and i dont want to switch over to opengl now, i dont have that much time, and i have exams
SO no open GL for now
"""

#This class provides an abstraction for the game scene
#maybe writing it like dis makes it a lot more abstract
#but it will make extending the game way easier
class Scene:
	def __init__(self, buff = False):
		self.HUD = HUD()
		self.objects = []
		self.props = []
		self.clouds = []
		self.cache = []
		self.buff = buff
		for i in range(100):
			self.clouds.append(FluffyCloud(GL.Translate([randint(-500000,500000),0,randint(-500000,500000)])))

	#isnt this beatiful overcomplication
	def render(self, screen, cam, width, height, plane, frame = 0):
		angles = plane.geteulers()
		for o in self.objects:
			if type(o) == type(Horizon(plane.pos)):
				o.render(screen, cam, width, height,angles[3])
			elif type(o) == type(Stars(1)):
				if plane.pos[1] > 60000:
					o.render(screen, cam, width, height)
			else:
				o.render(screen, cam, width, height)
		
		#first render stuff thats in the background, because I have no real clipping algorithm, so i have to first draw in the back so i draw over it
		#it reduces the problem to 2d in the plane, because height wont  matter that much
		self.props.sort(key = lambda p: np.linalg.norm(np.array([plane.pos[0],plane.pos[2]])-p.pos))
		for m in self.props[::-1]:
			if np.linalg.norm(np.array([plane.pos[0],plane.pos[2]])-m.pos) < sqrt(2*6371000)*sqrt(abs(plane.pos[1]))*0.9:
				m.render(screen, cam, width, height, plane.yawaxis)

		#rendering clouds takes approximately 12% of rendering time, if we only render clouds every second frame that saves approximateely 6%, which isn't much, but its fdun to implement
		#You can turn off buffering, because it makes the clouds a little bit shaky
		if not(frame%2) or ~self.buff:
			self.cache = []
			for c in self.clouds:
				c.render(screen, cam, width, height, plane.yawaxis, self.cache)
		else:
			for c in self.cache:
				pygame.draw.circle(screen,(255,255,255),(c[0],c[1]),c[2])

		self.HUD.render(screen, alt=plane.pos[1], gear = falsent if plane.gearlength > 0 else truent, Mach = plane.Mach, pitch = angles[0], roll = angles[3],\
			IAS = np.linalg.norm(plane.v)*sqrt(ISA(plane.pos[1])[1]/1.225), Engine=plane.Engine.isrunning, Heading = angles[2])
		
#Just a container
class Camera:
	#position and rotation are each a 1x3 array
	def __init__(self,pos,rot,width,height):
		#becuase we have the position and rotation
		#of the cam in the world frame, but we need to
		#transform the world angainst the camera so negative all of it
		self.angles = rot
		self.Translation = GL.TranslateNeg(pos)
		self.Rotation = GL.CamRot(-1*rot)
		self.Projection = GL.Projection(width/height,60,0.1,3000)

		#This just applys the transformation and projection to a point
		#No magic here jsut linear algebra
	def apply(self, position):
		relpos = np.dot(self.Translation,position)
		relpos = np.dot(self.Rotation,relpos)
		renderpos =np.dot(self.Projection,relpos)
		return relpos, renderpos

		#This gives back the unit vector in which the camera points
	def getunit(self):
		return np.dot(GL.CamRot(self.angles),np.array([0,0,-1,0]))[:3]

#Better CAm to be used with The newer Rotation stuff
#i jsut inherit it so i dont ahve to rewrite stuff :D
class BetterCam(Camera):
	def __init__(self,pos,rot,width,height):
		#becuase we have the position and rotation
		#of the cam in the world frame, but we need to
		#transform the world angainst the camera so negative all of it
		self.Translation = GL.TranslateNeg(pos)
		self.Rotation = rot
		self.fov = 60
		self.Projection = GL.Projection(width/height,self.fov,0.1,3000)

#This provides a base for all renderable objects
#Real Object Oriented Languages, would have the pussybility
#to make this classes mathods virtual, and the class abstract.
#So this class cant be build, but if something inherits from
#it has all functionality
class GameObject:

	#the off and rot are transformation matrixes from the origin
	def __init__(self, off, rot):
		pass

	#basic render function
	#Actually it does nothing, as you hopefully see
	#Thats why virtual functions exist
	def render(self, screen, cam, width, height):
		pass


#A light is nice and thats why it shoulf be its own
#object. COuld be a struct, but hey its python
class light:
	def __init__(self, pos, colour):
		self.pos = pos
		self.colour = colour

#Lets make a runway nigga
#it makes no blinky blinky, but we can add it
class Runway(GameObject):
	def __init__(self, off, rot):
		raw = np.genfromtxt("runway_lights-1.dat",delimiter=",",comments="#")
		#This is pretty self don't you think
		self.lights = []

		#we make a list of all the lights
		for i in range(raw.shape[0]):
			#we first convert to homogeneous coordinates and then apply
			#the Transformation matrixes
			#second argument is the colour as an rgb tuple
			self.lights.append(light(off.dot(rot).dot(np.array([raw[i,0],0.,-raw[i,1],1])),(raw[i,2],raw[i,3],raw[i,4])))

	#dis where we overwrite the useless function from the game object
	#Though i dont use vectorization, it shouldnt really impact speed, because the math still happens in numpy, just the loop is in python
	def render(self, screen, cam, width, height):
		#just go through all the lights, apply the cam and
		#then dont render it if its behind the cam
		#nothing that lays behind you should matter to you
		for l in self.lights:
			#to be honest: this is like the worst time efficiency in the code
			relpos, renderpos = cam.apply(l.pos)

			#Like at the moment the render position is not homogeneous
			#so we make it homogeneous, but only if its not to close to 0
			if relpos[2]<0 and not(-0.001<renderpos[3]<0.001):
				#make the coordinate homogeneous
				#make it in numpy for the extra 0.000001s of speed
				renderpos = np.divide(renderpos,renderpos[3])
				#then we make it to x,y space and invert y because python is a
				#special child
				#we could also precompute width and height divided by two to shave off more time
				#Actually division has bad time complexity, but it wont really affect our code since it depends on the number of
				#digits so it isnt too bad
				x = (1+renderpos[0])*width/2/renderpos[2]
				y = (1-renderpos[1])*height/2/renderpos[2]
				# now we cutoff if its outside the screen, because that saves time
				if 0<=x<=width and 0<=y<=height:
					pygame.draw.circle(screen,l.colour,(int(x),int(y)),int(max(1,(5000-np.linalg.norm(relpos[:3]))/1500)))
#These mountains are ugly, but i cant figure out how to do concave shells\
class Mountain(GameObject):
	def __init__(self, off, rot, height,seed = 0):
		#https://i.redd.it/f0tl89cy7e311.jpg
		self.verts = []
		self.greys = [(180,180,180),(150,150,150),(200,200,200),(120,120,120)]
		self.seed = seed
		self.pos = np.array([off[0,3],off[2,3]])
		self.verts.append(off.dot(rot.dot(np.array([0,height,0,1]))))
		self.verts.append(off.dot(rot.dot(np.array([height*1.2+randint(-2000,2000),0,0+randint(-2000,2000),1]))))
		self.verts.append(off.dot(rot.dot(np.array([-height*1.3+randint(-2000,2000),0,height*1.3+randint(-2000,2000),1]))))
		self.verts.append(off.dot(rot.dot(np.array([-height*1.3+randint(-2000,2000),0,-height*1.3+randint(-2000,2000),1]))))

	def render(self, screen, cam, width, height, spam):
		triangles = [[0,3,1],[0,1,2],[0,2,3]]
		renders = []
		for t in triangles:
			r = []
			for v in t:
				relpos, renderpos = cam.apply(self.verts[v])
				if relpos[2]<0 and not(-0.001<renderpos[3]<0.001):
					#make the coordinate homogeneous
					#make it in numpy for the extra 0.000001s of speed
					renderpos = np.divide(renderpos,renderpos[3])
					#then we make it to x,y space and invert y because python is a
					#special child
					#we could also precompute width and height divided by two to shave off more time
					#Actually division has bad time complexity, but it wont really affect our code since it depends on the number of
					#digits so it isnt too bad
					x = (1+renderpos[0])*width/2/renderpos[2]
					y = (1-renderpos[1])*height/2/renderpos[2]
					# now we cutoff if its outside the screen, because that saves time
					if -400<=x<=width+400 and -400<=y<=height+4000:
						r.append(np.array([x,y]))
				if len(r)>=0:
					renders.append([r,np.linalg.norm(relpos[:3])])

		renders.sort(key = lambda p: abs(p[1]), reverse = falsent)

		for i in range(len(renders)):
			if len(renders[i][0])>=3:
				pygame.draw.polygon(screen, self.greys[(self.seed+i)%len(self.greys)],renders[i][0])

#Tiny trees
class Baum(GameObject):
	def __init__(self, off):
		self.stemcolour = (randint(120,150),randint(50,80),randint(0,30))
		self.leafcolour = (randint(0,130),randint(100,200),randint(0,70))
		self.stemvertices = []
		self.pos = np.array([off[0,3],off[2,3]])
		self.stemvertices.append(off.dot(np.array([-0.5,0,0,1])))
		self.stemvertices.append(off.dot(np.array([0.5,0,-0.25,1])))
		self.stemvertices.append(off.dot(np.array([0.5,0,0.25,1])))
		height = random()*50+5
		self.stemvertices.append(off.dot(np.array([0,height,0,1])))

		self.radius = height/2.5*(1+(random()-0.5))
		self.leafcentre = off.dot(np.array([0,height,0,1]))

	def render(self, screen, cam, width, height, axisperp):
		if np.linalg.norm(cam.apply(np.array([self.pos[0],0,self.pos[1],1]))[1][:3]) > 10000:
			return
		renders = []
		for v in self.stemvertices:
			relpos, renderpos = cam.apply(v)
			if relpos[2]<0 and not(-0.001<renderpos[3]<0.001):
				#make the coordinate homogeneous
				#make it in numpy for the extra 0.000001s of speed
				renderpos = np.divide(renderpos,renderpos[3])
				#then we make it to x,y space and invert y because python is a
				#special child
				#we could also precompute width and height divided by two to shave off more time
				#Actually division has bad time complexity, but it wont really affect our code since it depends on the number of
				#digits so it isnt too bad
				x = (1+renderpos[0])*width/2/renderpos[2]
				y = (1-renderpos[1])*height/2/renderpos[2]
				# now we cutoff if its outside the screen, because that saves time
				if -400<=x<=width+400 and -400<=y<=height+4000:
					renders.append(np.array((x,y)))
		if len(renders)>=3:
			pygame.draw.polygon(screen,self.stemcolour,convexHull(renders))

		relpos, renderpos = cam.apply(self.leafcentre)
		if relpos[2]<0 and not(-0.001<renderpos[3]<0.001):
			#make the coordinate homogeneous
			#make it in numpy for the extra 0.000001s of speed
			renderpos = np.divide(renderpos,renderpos[3])
			#then we make it to x,y space and invert y because python is a
			#special child
			#we could also precompute width and height divided by two to shave off more time
			#Actually division has bad time complexity, but it wont really affect our code since it depends on the number of
			#digits so it isnt too bad
			x = (1+renderpos[0])*width/2/renderpos[2]
			y = (1-renderpos[1])*height/2/renderpos[2]
			# now we cutoff if its outside the screen, because that saves time
			if -400<=x<=width+400 and -400<=y<=height+4000:
				#now it get a little bit more complicated
				#we take a vector perpendicular to the viewing vector, which in our case is the yaw or pitchaxis, doesnt matter
				#if we then find a point on the sphere, which vector to the origin is exactly that vect, we can find the radius of the projected
				#sphere, by taking the distance between that projectected point and the centre, which we computed before
				relpos2, renderpos2 = cam.apply(np.append((self.leafcentre[:3]+self.radius*axisperp),[1]))
				renderpos2 = np.divide(renderpos2,renderpos2[3])
				x2 = (1+renderpos2[0])*width/2/renderpos2[2]
				y2 = (1-renderpos2[1])*height/2/renderpos2[2]
				radius = np.linalg.norm(np.array([x,y])-np.array([x2,y2]))
				pygame.draw.circle(screen, self.leafcolour, (int(x),int(y)), int(radius))

class FluffyCloud(GameObject):
	def __init__(self, off):
		self.vertices = []
		self.pos = np.array([off[0,3],off[2,3]])
		self.radii = []

		self.height = randint(7000,15000)

		for i in range(randint(5,10)):
			self.radii.append(randint(500,1000))
			self.vertices.append(off.dot(np.array([randint(-1000,1000),self.height+randint(-100,100),randint(-1000,1000),1])))

	def render(self, screen, cam, width, height, axisperp, cache):
		if cam.apply(np.array([self.pos[0],0,self.pos[1],1]))[0][2] > -1:
			return
		#we calculate a unit radius to save time
		relpos, renderpos = cam.apply(np.array([self.pos[0],self.height,self.pos[1],1]))
		renderpos = np.divide(renderpos,renderpos[3])
		x = (1+renderpos[0])*width/2/renderpos[2]
		y = (1-renderpos[1])*height/2/renderpos[2]
		#Read up on BAum
		relpos2, renderpos2 = cam.apply(np.append((np.array([self.pos[0],self.height,self.pos[1]])+axisperp),[1]))
		renderpos2 = np.divide(renderpos2,renderpos2[3])
		x2 = (1+renderpos2[0])*width/2/renderpos2[2]
		y2 = (1-renderpos2[1])*height/2/renderpos2[2]
		radius = np.linalg.norm(np.array([x,y])-np.array([x2,y2]))

		for i in range(len(self.vertices)):
			relpos, renderpos = cam.apply(self.vertices[i])
			renderpos = np.divide(renderpos,renderpos[3])
			x = (1+renderpos[0])*width/2/renderpos[2]
			y = (1-renderpos[1])*height/2/renderpos[2]
			if -100<x<width+100 and -100<y<height+100:
				cache.append([int(x),int(y),int(radius*self.radii[i])])
				pygame.draw.circle(screen,(255,255,255),(int(x),int(y)),int(radius*self.radii[i]))




class Stars(Runway):
	def __init__(self,len):
		self.lights = []

		for i in range(len):
			#we use a normal distribution to kinda getaround the high concentration of stars on the edge of space
			#Its not even close to physically correct, but looks nice
			self.lights.append(light(np.array([700000*(random() -0.5),400000*random() +100000,700000*(random() -0.5),1]),(255,255,255)))

#Duhh its not really a mountain, but a pyramide
class Mountain(GameObject):
	def __init__(self, off, rot, height,seed = 0):
		#https://i.redd.it/f0tl89cy7e311.jpg
		self.verts = []
		self.greys = [(180,180,180),(150,150,150),(200,200,200),(120,120,120)]
		self.seed = seed
		self.pos = np.array([off[0,3],off[2,3]])
		self.verts.append(off.dot(rot.dot(np.array([0,height,0,1]))))
		self.verts.append(off.dot(rot.dot(np.array([height*1.2,0,0,1]))))
		self.verts.append(off.dot(rot.dot(np.array([-height*1.3,0,height*1.3,1]))))
		self.verts.append(off.dot(rot.dot(np.array([-height*1.3,0,-height*1.3,1]))))

	def render(self, screen, cam, width, height, spam):
		triangles = [[0,3,1],[0,1,2],[0,2,3]]
		renders = []
		for t in triangles:
			r = []
			for v in t:
				relpos, renderpos = cam.apply(self.verts[v])
				if relpos[2]<0 and not(-0.001<renderpos[3]<0.001):
					#make the coordinate homogeneous
					#make it in numpy for the extra 0.000001s of speed
					renderpos = np.divide(renderpos,renderpos[3])
					#then we make it to x,y space and invert y because python is a
					#special child
					#we could also precompute width and height divided by two to shave off more time
					#Actually division has bad time complexity, but it wont really affect our code since it depends on the number of
					#digits so it isnt too bad
					x = (1+renderpos[0])*width/2/renderpos[2]
					y = (1-renderpos[1])*height/2/renderpos[2]
					# now we cutoff if its outside the screen, because that saves time
					if -400<=x<=width+400 and -400<=y<=height+4000:
						r.append(np.array([x,y]))
				if len(r)>=0:
					renders.append([r,np.linalg.norm(relpos[:3])])

		renders.sort(key = lambda p: abs(p[1]), reverse = falsent)

		for i in range(len(renders)):
			if len(renders[i][0])>=3:
				pygame.draw.polygon(screen, self.greys[(self.seed+i)%len(self.greys)],renders[i][0])

class Horizon(GameObject):
	def setpos(self, cpos):
		#MAgic number
		self.r = sqrt(2*6371000)*sqrt(abs(cpos[1]))+50
		self.p = np.array([cpos[0],cpos[1]])

	def __init__(self, cpos):
		self.setpos(cpos)

	def update(self, cpos):
		self.setpos(cpos)

	#sadly we cant use the old render method so just make a new one
	def render(self, screen, cam, width, height, roll):
		
		#Maybe this number was determined by guessing but you will never know ¯\_(ツ)_/¯
		down = 2000*np.array([sin(radians(roll)),cos(radians(roll))])
		ps = []
		for angle in range(0,360,3):
			a = radians(angle)
			tmp = self.p + GL.Rot2D(a).dot(np.array([self.r,0]))
			ps.append(np.array([tmp[0],0,tmp[1],1]))

		renders = []
		for d in ps:
			relpos, renderpos = cam.apply(d)
			renderpos = np.divide(renderpos,renderpos[3])
			#then we make it to x,y space and invert y because python is a
			#special child
			x = (1+renderpos[0])*width/2/renderpos[2]
			y = (1-renderpos[1])*height/2/renderpos[2]
			if relpos[2] < 0 and -300<=x<=width+300 and -500<=y<=height+500:
				renders.append((x,y))

		if len(renders)>=2:
			m = (renders[-1][1]-renders[0][1])/(renders[-1][0]-renders[0][0])
			d = 2*sqrt(width**2+height**2)
			p1 = np.array([renders[0][0]-d,renders[0][1]-m*d])
			p2 = np.array([renders[0][0]+d,renders[0][1]+m*d])
		
			rend = [p1,p2,p2+down,p1+down]
			pygame.draw.polygon(screen,(76,70,50),rend)

#make the sky darker as we approach space
#not that spectacular
def colourfade(alt):
	sky = (135, 206, 250)
	maxalt = 90000
	if alt > maxalt:
		return (0,0,0)
	else:
		d = maxalt-alt
		return (0+int(sky[0]*d/maxalt),0+int(sky[1]*d/maxalt),0+int(sky[2]*d/maxalt))

plane = X15(np.array([0.,10000.,110000]))
plane.roll(0.01,1) #Otherwise the artifical horizon is fucked up, dont ask me why

width = 1000
height = 800

pygame.init()
screen = pygame.display.set_mode((width,height))

scene = Scene()
cam = BetterCam(plane.pos,plane.getrot(),width,height)
#if you dont want to have the runway transformed give it 4*4 Identity matrixes
#If you have patience and a fucking strong pc, you could build the whole world outta it
#Last added is also last rendered
scene.objects.append(Stars(300))
scene.objects.append(Horizon(plane.pos))
scene.objects.append(Runway(np.identity(4),np.identity(4)))

for i in range(50):
	d = randint(100000,500000)
	angle = randint(0,360)
	scene.props.append(Mountain(GL.Translate([d*cos(radians(angle)),-200,d*sin(radians(angle))]),np.identity(4),randint(3000,6500),seed = 1))

for i in range(500):
	d = randint(0,100000)
	angle = randint(0,360)
	scene.props.append(Baum(GL.Translate([d*cos(radians(angle)),-200,d*sin(radians(angle))])))

#Some fonts
myfont = pygame.font.SysFont("monospace", 23)
myfont2 = pygame.font.SysFont("monospace", 10)

#For physics
lastt = 0
lastgearswitch = 0
fr = 0
while falsent:
	#More math for physics
	curt = pygame.time.get_ticks()
	keys = pygame.key.get_pressed()
	dt = (curt-lastt)/1000 #makey makey deytey
	cam = BetterCam(plane.pos,plane.getrot(),width,height)
	plane.update(dt,keys[pygame.K_b])
	#Ugly HAck 
	screen.fill(colourfade(plane.pos[1]) if plane.geteulers()[0]>-(cam.fov*(sqrt(width**2+height**2)/height)/2) else (76,70,50))
	scene.render(screen, cam, width, height, plane, frame = fr)

	#if you remove this you will have to study materials forever
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			sys.exit()

	#there  must be a better way to do this, but im not that good with python
	#so i'll leave it like this
	for i in range(len(scene.objects)):
		if type(scene.objects[i]) == type(Horizon(plane.pos)):
			scene.objects[i].update(plane.pos)

	#Stuff to control
	if keys[pygame.K_w]:
		plane.pitch(0.5,dt)
	if keys[pygame.K_s]:
		plane.pitch(-0.5,dt)

	if keys[pygame.K_a]:
		plane.yaw(0.5,dt)
	if keys[pygame.K_d]:
		plane.yaw(-0.5,dt)

	if keys[pygame.K_q]:
		plane.roll(0.5,dt)
	if keys[pygame.K_e]:
		plane.roll(-0.5,dt)

	if keys[pygame.K_g]:
		#make the gear less flippy
		print((pygame.time.get_ticks()-500) > lastgearswitch)
		if (pygame.time.get_ticks()-500) > lastgearswitch:
			plane.switchgear()
			lastgearswitch = pygame.time.get_ticks()

	if keys[pygame.K_SPACE]:
		plane.Engine.ignite()
	if debug:
		screen.blit(myfont.render(str(round(np.linalg.norm(plane.v)))+"m/s",1,(255,0,0)),(10,50))
		screen.blit(myfont.render(str(round(plane.pos[2]))+"m",1,(255,0,0)),(10,70))
		screen.blit(myfont.render(str(round(plane.pos[1]))+"m",1,(255,0,0)),(10,90))
		screen.blit(myfont.render(str(round(plane.geteulers()[1]))+"deg",1,(255,0,0)),(10,110))
		screen.blit(myfont.render(str(round(plane.geteulers()[2]))+"deg",1,(255,0,0)),(10,130))
		screen.blit(myfont.render(str(round(plane.geteulers()[3]))+"deg",1,(255,0,0)),(10,150))

		screen.blit(myfont.render(str(round(plane.getveleulers()[0]))+"deg",1,(255,0,0)),(10,170))
		screen.blit(myfont.render(str(round(plane.getveleulers()[1]))+"deg",1,(255,0,0)),(10,190))
	pygame.display.update()
	lastt = curt
	fr+=1
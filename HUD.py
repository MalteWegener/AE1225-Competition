"""
@author: Malte Wegener
"""

#pragma once
import pygame
from math import *
from GL import Rot2D
import numpy as np
import GL


#TODO, make an abstract base class for Gauges, so it looks like OOP

class AltitudeGauge():
	def __init__(self, offset):
		self.offset = np.array(offset)
		self.gaug = pygame.image.load("Altimeter.gif")
		self.gaug = pygame.transform.scale(self.gaug,(200,200))
	def render(self,screen, alt):
		#Convert to feet
		altft = alt*3.28084
		#get pointer angles, but not the good kind of pointers
		#I first had to research how an altitude thing actually displays altitude
		#ok i make 3 pointerthingys, because we go high
		ang10k = 2*pi*altft/100000%(2*pi)
		ang1k = 2*pi*altft/10000%(2*pi)
		ang100 = 2*pi*altft/1000%(2*pi)
		screen.blit(self.gaug,self.offset)
		#the most big pointer is the smallest drawn
		#This is selfexplanatory, i hope
		maxlen = 45*sqrt(2)*np.array([0,-1])
		grey=(230,130,130)
		centre = self.offset+np.array((100,100))

		#10k pointer
		pygame.draw.line(screen,grey,centre, centre+Rot2D(ang10k).dot(1/3*maxlen),5)
		#1k pointer
		pygame.draw.line(screen,grey,centre, centre+Rot2D(ang1k).dot(2/3*maxlen),4)
		#100 pointer
		pygame.draw.line(screen,grey,centre, centre+Rot2D(ang100).dot(maxlen),3)

class MachMeter():
	def __init__(self, offset):
		self.offset = np.array(offset)
		self.radius = 60

	def render(self, screen, Mach):
		font = pygame.font.SysFont("monospace", 11)
		font2 = pygame.font.SysFont("monospace", 20)
		maxlen = self.radius*np.array([0,-1])
		#This is some magic number, because i dont know why it wont work without it
		pygame.draw.circle(screen,(110,110,110),self.offset+np.array([5,5]),self.radius+30)
		pygame.draw.circle(screen,(90,90,90),self.offset+np.array([5,5]),self.radius+30,5)
		screen.blit(font2.render("Mach",1,(255,255,255)),self.offset-np.array(font2.size("Mach"))/2+np.array([5,5]))
		for ang in range(0,12):
			tmp = self.offset+Rot2D(2*pi/12*ang).dot(maxlen-np.array(font.size(str(ang)))/2)
			screen.blit(font.render(str(ang),1,(255,255,255)),(int(tmp[0]),int(tmp[1])))

		centre = self.offset+np.array([5,5])
		angle = min(2*pi,Mach/12*2*pi)
		pygame.draw.line(screen,(255,255,0),centre,centre+Rot2D(angle).dot(5/4*maxlen),3)

#HAH this is just inheritance without saying its inheritance
#MAy be sketchy
class IASMeter():
	def __init__(self, offset):
		self.offset = np.array(offset)
		self.radius = 60

	def render(self, screen, IAS):
		font = pygame.font.SysFont("monospace", 11)
		font2 = pygame.font.SysFont("monospace", 20)
		maxlen = self.radius*np.array([0,-1])
		#This is some magic number, because i dont know why it wont work without it
		pygame.draw.circle(screen,(110,110,110),self.offset+np.array([5,5]),self.radius+30)
		pygame.draw.circle(screen,(90,90,90),self.offset+np.array([5,5]),self.radius+30,5)
		screen.blit(font2.render("IAS",1,(255,255,255)),self.offset-np.array(font2.size("IAS"))/2+np.array([5,5]))
		screen.blit(font2.render("knts",1,(255,255,255)),self.offset-np.array(font2.size("knts"))/2+np.array([5,25]))
		for ang in range(0,18):
			tmp = self.offset+Rot2D(2*pi/20*ang).dot(maxlen-np.array(font.size(str(ang)))/2)
			screen.blit(font.render(str(ang*50),1,(255,255,255)),(int(tmp[0]),int(tmp[1])))

		centre = self.offset+np.array([5,5])
		angle = min(15/18*2*pi,IAS/(50*17)*2*pi)
		pygame.draw.line(screen,(255,255,0),centre,centre+Rot2D(angle).dot(5/4*maxlen),3)

class HeadingThing:
	def __init__(self, offset):
		self.offset = np.array(offset)
		scale = 8
		self.size = 180
		self.rect = pygame.Surface((self.size,self.size))

		#I HArdcode that, because its nonregular, i could do the symetry stuff with a loop, buit that would have no
		#real benefit for now
		# (/_\) Where is the ugly code?
		# (\*_*/) There is the ugly code!
		self.vertices = []
		self.vertices.append(np.array([0,-7])*scale)
		self.vertices.append(np.array([1,-1])*scale)
		self.vertices.append(np.array([6,0])*scale)
		self.vertices.append(np.array([6,1])*scale)
		self.vertices.append(np.array([2,2])*scale)
		self.vertices.append(np.array([1,6])*scale)
		self.vertices.append(np.array([3,7])*scale)
		self.vertices.append(np.array([1,8])*scale)
		self.vertices.append(np.array([0,8])*scale)

		self.vertices.append(np.array([-1,8])*scale)
		self.vertices.append(np.array([-3,7])*scale)
		self.vertices.append(np.array([-1,6])*scale)
		self.vertices.append(np.array([-2,2])*scale)
		self.vertices.append(np.array([-6,1])*scale)
		self.vertices.append(np.array([-6,0])*scale)
		self.vertices.append(np.array([-1,-1])*scale)

	def render(self,screen,heading):
		verts = []
		for i in range(len(self.vertices)):
			verts.append(Rot2D(radians(heading)).dot(self.vertices[i])+np.array([self.size,self.size])/2)

		self.rect.fill((15,15,15))
		pygame.draw.polygon(self.rect,(255,255,255),verts,3)
		pygame.draw.line(self.rect,(255,255,0),Rot2D(radians(heading)).dot(np.array([0,75]))+np.array([self.size,self.size])/2,Rot2D(radians(heading)).dot(np.array([0,-75]))+np.array([self.size,self.size])/2,4)
		pygame.draw.circle(self.rect,(150,150,150),(int(self.size/2),int(self.size/2)),int(sqrt(2)*self.size),int((sqrt(2)*self.size-80)))
		for i in range(0,360,10):
			pygame.draw.line(self.rect,(255,255,255),Rot2D(radians(i)).dot(np.array([0,70]))+np.array([self.size,self.size])/2,Rot2D(radians(i)).dot(np.array([0,(80 if i%30 else 90)]))+np.array([self.size,self.size])/2,2)
		screen.blit(self.rect, self.offset)
		

class Instructor():
	def __init__(self, offset):
		self.offset = offset
		self.rect = pygame.Surface((300,100))
		font = pygame.font.SysFont("monospace", 15)
		self.steps = []
		self.steps.append(font.render("1. Ignite the Engine and",1,(255,255,255)))
		self.steps.append(font.render("   hold a pitch of 80 deg",1,(255,255,255)))
		self.steps.append(font.render("2. Just follow the Flow of",1,(255,255,255)))
		self.steps.append(font.render("   the Aircraft",1,(255,255,255)))
		self.steps.append(font.render("3. Try to hit the Runway,",1,(255,255,255)))
		self.steps.append(font.render("   dont forget Landing Gear",1,(255,255,255)))
	def render(self,screen):
		n = 0
		for s in self.steps:
			self.rect.blit(s,(0,n*16))
			n+=1
		screen.blit(self.rect, self.offset)

#pretty basic and ugly articicial Horizon
#Tjis was seriously harder than all of the rest combined twice squared factorial
#Im done with this adn it took 7 hours and 5 trys
class ArtificialHori():
	def __init__(self, offset):
		self.offset = offset
		self.font = pygame.font.SysFont("monospace", 11)
		self.width = 200
		self.height = 200
		self.rect = pygame.Surface((self.width,self.height))

	def render(self, screen, pitch, roll):
		#pitch is between -90 and 90
		#roll between -180 and 180
		#while writing this i became more and more confused what i am even doing, just accept the code
		#And the Artificial Horizon is ugly as fuck
		font = pygame.font.SysFont("monospace", 13)
		factor = 3

		#a vector pointing right in the transformed horizontal plane
		right = 10*np.array([cos(radians(roll)),-sin(radians(roll))])

		#vertices of ground and the center of the screen
		grd = [np.array([0,180*factor/2]),np.array([0,180*factor]),np.array([180*factor,180*factor]),np.array([180*factor,180*factor/2])]
		ctr = np.array([180*factor/2,180*factor/2-pitch*factor])

		#end of the lines
		ll = []
		lr = []

		for i in range(0,180,10):
			#make them not full over screen, and every 20 degrees the thing is longer
			ll.append(np.array([180*factor/2 - (40 if i %20 else 20),i*factor]))
			lr.append(np.array([180*factor/2 + (40 if i %20 else 20),i*factor]))

		#After this this is trasnformation
		#What comes after this is the least space efficient thing ever
		grdreltoctrrot = [] #ground vertices relative to centre and rotatet
		for g in grd:
			grdreltoctrrot.append(Rot2D(radians(-roll)).dot(g-ctr))

		llreltoctrrot = []
		lrreltoctrrot = []
		for i in range(len(ll)):
			llreltoctrrot.append(Rot2D(radians(-roll)).dot(ll[i]-ctr))
			lrreltoctrrot.append(Rot2D(radians(-roll)).dot(lr[i]-ctr))

		# now we have to make that to screen coordintes, somehow, i am more like trying out what works
		scrpos = []
		for g in grdreltoctrrot:
			scrpos.append(g+np.array([self.width/2,self.height/2]))

		llscrpos = []
		lrscrpos = []
		for i in range(len(llreltoctrrot)):
			llscrpos.append(llreltoctrrot[i]+np.array([self.width/2,self.height/2]))
			lrscrpos.append(lrreltoctrrot[i]+np.array([self.width/2,self.height/2]))

		#Now we draw stuff
		self.rect.fill((0,0,255))
		pygame.draw.polygon(self.rect,(0,255,0),scrpos)
		pygame.draw.line(self.rect,(0,0,0),scrpos[0],scrpos[3],3)

		for i in range(len(llscrpos)):
			pygame.draw.line(self.rect,(0,0,0),llscrpos[i],lrscrpos[i],3)
			self.rect.blit(font.render(str(90-i*10),1,(0,0,0)),(llscrpos[i]+lrscrpos[i])/2)

		#And the airplane indicator thing
		pygame.draw.circle(self.rect,(120,120,0),(int(self.width/2),int(self.height/2)),5)
		#I seriously dont know why it looks perforated
		pygame.draw.circle(self.rect,(0,0,0),(int(self.width/2),int(self.height/2)),int(sqrt((self.width/2)**2+(self.height/2)**2)),35)
		#Draw some roll indication lines

		pygame.draw.line(self.rect,(255,255,0),np.array([self.width/2,self.height/2])-10*right,np.array([self.width/2,self.height/2])-3*right,3)
		pygame.draw.line(self.rect,(255,255,0),np.array([self.width/2,self.height/2])+10*right,np.array([self.width/2,self.height/2])+3*right,3)

		screen.blit(self.rect,self.offset-np.array([self.width/2,self.height/2]))


class HUD():
	def __init__(self):
		self.background = pygame.image.load("cockpitbackground.gif")
		self.Altimeter =  AltitudeGauge([200,470])
		self.MachMeter = MachMeter([100,700])
		self.gearlightpos = (450,750)
		self.Enginelightpos = (300,750)
		self.gearlightradius = 40
		self.ArtificialHori = ArtificialHori((500,550))
		self.IASMeter = IASMeter([900,700])
		self.myfont = pygame.font.SysFont("monospace", 23)
		self.HeadingThing = HeadingThing((630,480))
		self.Instructor = Instructor((500,700))


	#we could use **kwargs, to make it more scalable, but we actually just use a predefined number of arguments
	#so we used keyword arguments for better easy
	def render(self,screen, alt = 0 , gear = False, Mach = 0, pitch = 0, roll = 0, IAS = 0, Engine = False, Heading=0):
		screen.blit(self.background,[0,0])
		self.Altimeter.render(screen, alt)
		#I like one liners
		screen.blit(self.myfont.render("Gear",1,(0,0,0)),(self.gearlightpos[0]-self.myfont.size("Gear")[0]/2,self.gearlightpos[1]-self.gearlightradius-self.myfont.size("Gear")[1]))
		screen.blit(self.myfont.render("Rocket Engine",1,(0,0,0)),(self.Enginelightpos[0]-self.myfont.size("Rocket Engine")[0]/2,self.Enginelightpos[1]-self.gearlightradius-self.myfont.size("Rocket Engine")[1]))
		if gear:
			pygame.draw.circle(screen,(0,255,0),self.gearlightpos,self.gearlightradius)
		else:
			pygame.draw.circle(screen,(255,0,0),self.gearlightpos,self.gearlightradius)

		if Engine:
			pygame.draw.circle(screen,(0,255,0),self.Enginelightpos,self.gearlightradius)
		else:
			pygame.draw.circle(screen,(255,0,0),self.Enginelightpos,self.gearlightradius)
		self.MachMeter.render(screen,Mach)
		self.IASMeter.render(screen,IAS*1.94384)
		self.HeadingThing.render(screen,Heading)
		self.ArtificialHori.render(screen,pitch,roll)
		self.Instructor.render(screen)
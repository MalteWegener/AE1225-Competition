"""
@author: Malte Wegener
"""

#pragma once
import math #For meth
import matplotlib.pyplot as plt #For Graphs
import os #For Clearing the Console

#constants
R = 287
g0 = 9.81


#General class for producing different layers, provides expandabilty
class Layer:
	#calculates the condition of the previous layer at the boundary to the new layer
	def __init__(self, a, h0, prevlayer):
		#That shit better self explanatory or you retarded
		self.a = a
		self.h0 = h0
		self.T0 = prevlayer.T0 + prevlayer.a *(h0 - prevlayer.h0)

		#isothermal or the other kind of bs
		if prevlayer.a == 0:
			self.p0 = prevlayer.p0 * math.exp(-g0/(R*prevlayer.T0)*(h0-prevlayer.h0))
		else:
			self.p0 = prevlayer.p0 * math.pow(self.T0/prevlayer.T0,-g0/(R*prevlayer.a))

		#ideal gas law bro
		self.rho0 = self.p0/(R * self.T0)

	#function for a gradient Layer
	def gradient_layer(self, alt):
		T = self.T0 + self.a *(alt-self.h0)
		press = self.p0 * math.pow(T/self.T0,-g0/(self.a*R))
		rho = press/(R*T)
		return [press,rho,T]
		
	#Wow ISothermal Layer, Big Deal
	def isothermal_layer(self, alt):
		T = self.T0
		p = press = self.p0 * math.exp(-g0/(R*T)*(alt-self.h0))
		rho = press/(R*T)
		return [press,rho,T]

	#get the state of the Atmosphere at a specific height in the layer
	#<returns>
	#[pressure, density, Temperature]
	#<\returns>
	def get_state(self, alt):
		return self.gradient_layer(alt) if self.a != 0 else self.isothermal_layer(alt)

# derived class for the lowest Layer in this case the Troposphere
class Troposphere(Layer):
	def __init__(self):
		#Dis all basic Highschool math and Physics
		self.a = -0.0065
		self.h0 = 0
		self.T0 = 288.15
		self.p0 = 101325
		self.rho0 = self.p0/(R * self.T0)

	#Changing T0 results in changing rho0, but nothing else
	def change_T0(self, new):
		self.T0 = new
		self.rho0 = self.p0/(R * self.T0)

def ISA(alt):

	#Declaration of the Atmosphere
	Atmosphere = []
	#Definitions for the Different Layers, Starting height and Gradient
	#Could be read from a csv or an excel sheet for all kinds of Atmospheres
	defin = [[0,-0.0065],[11000,0],[20000,0.001],[32000,0.0028],[47000,0],[51000,-0.0028],[71000,-0.002],[84852,0]]

	#Add the Troposphere, only non dependant Layer
	Atmosphere.append(Troposphere())

	#Addd all the Other Layers, based on the Previous ones
	for i in range(1,len(defin)):
		Atmosphere.append(Layer(defin[i][1], defin[i][0], Atmosphere[i-1]))

	#TODO: make sleeker
	index = 0
	if alt > 84000:
		return[0,0,0]

	for i in range(len(Atmosphere)):
		if alt >= Atmosphere[i].h0:
			index = i
		else:
			break

	return Atmosphere[index].get_state(alt)

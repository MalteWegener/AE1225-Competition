#pragma once
class Vec3Ph
{
private:
	float data[];
public:
	Vec3Ph();
	Vec3Ph(float x, float y, float z);
	float operator[](int index) { return data[index]; }
	Vec3Ph& operator+(Vec3Ph);
	~Vec3Ph();
};


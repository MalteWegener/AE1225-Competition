import numpy as np
from math import atan,degrees
#Giftwrapping algorithm
#https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRrd1dp36Evi-_faq6kJVp6_3xLMmQycXi89xhlL68U6lINXmP
def convexHull(S):
	start = min(S, key = lambda p: p[0])
	p = []
	while True:
		p.append(start)
		end = S[0]

		for i in range(1,len(S)):
			if (end[0] == start[0] and end[1] == start[1]) or np.cross(end-start,S[i]-start) > 0:
				end = S[i]
		start = end

		if end[0] == p[0][0] and end[1] == p[0][1]:
			break
	return p
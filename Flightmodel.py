"""
@author: Malte Wegener
"""

#pragma once
import numpy as np
import GL
from math import *
from ISA import ISA

#More abstraction means more fun
#just a cointainer for the airfoil, which can be loaded from files
class Airfoil():
	def __init__(self, datfile):
		file = open(datfile+".arf")
		lines = file.readlines()
		self.CD0 = float(lines[0].split()[0])
		self.CM = float(lines[1].split()[0])
		self.CL0 = float(lines[2].split()[0])
		self.lowerstall = float(lines[3].split()[0])
		self.upperstall = float(lines[4].split()[0])
		self.dcl = float(lines[5].split()[0])

	#This is just math
	def GetCL(self, alpha):
		#pretty basic psot stall behaviour
		if alpha >= self.upperstall:
			return 0.6
		if alpha <= self.lowerstall:
			return -0.6

		return self.dcl * alpha + self.CL0

#Next after an airfoil there coems a wing
class Wing():
	def __init__(self, datfile, span, MAC, oswald):
		self.Airfoil = Airfoil(datfile)
		self.Aspect = span/MAC
		self.MAC = MAC
		self.oswald = oswald
		self.Area = span * MAC

	#Returns L, D, M
	def getAerodynamics(self, q, alpha, Mach):
		L = self.Airfoil.GetCL(alpha) * q * self.Area
		D = (self.Airfoil.CD0+self.Airfoil.GetCL(alpha)**2/(pi*self.oswald*self.Aspect)) * q * self.Area
		M = q*self.Airfoil.CM*self.MAC*self.Area
		return L,D,M

#We need power to go anywhere
#This code up until now was elf documenting
class Engine():
	def __init__(self):
		self.thrust = 310000
		self.burntime = 63
		self.burned = 0
		self.isrunning = False

	def ignite(self):
		self.isrunning = True

	def getThrust(self,dt):
		self.burned += dt if self.isrunning else 0
		if self.burned >= self.burntime:
			self.isrunning = False
		return self.thrust if self.isrunning and self.burned < self.burntime else 0

#we use SI Base units like civilized people
#This might look complicated, but its pretty straight forawrd
class X15():
	def __init__(self, pos):
		#First we define a bunch of stuff
		self.pos = pos
		self.macoff = -0.8 #m
		self.Tailoffset = 7 #m
		self.Engine = Engine()
		self.mass = 15420 #kg
		self.Wing = Wing("test",6.71,3.01,0.95)
		self.Tail = Wing("test",1,0.5,0.95)
		self.yawaxis = np.array([0,-1.,0])
		self.pitchaxis = np.array([1.,0,0])
		self.rollaxis = np.array([0,0,-1.])
		self.v = 277 * self.rollaxis
		self.Mach = 0 #we could calculate her, but it is conditional from other stuff and instatnly gets updated
		self.gearlength = 0

	#some basic gear logic
	def switchgear(self):
		if self.gearlength == 0:
			self.gearlength = 1.5
		else:
			self.gearlength = 0

	#Ok im actually proud of this part now, we rotate the coordinate system of the plane with the plane and likt this we can achieve this real
	#Feeling of controlling an aircraft
	#The universal Rotation matrix around a unit vector is on wikipedia, im not that good at linear algebra, but i think
	#This part should count more like the approach to the problem rather than where i got the MAtrix
	#We ofcousre could just use trigonometry to rotate the coordinate system, but i like matrices, because they are so conscise Dunno how to write that word
	def pitch(self,angle, dt):
		self.yawaxis = GL.UniversalRotationDeg(angle,self.pitchaxis).dot(self.yawaxis)
		self.rollaxis = GL.UniversalRotationDeg(angle,self.pitchaxis).dot(self.rollaxis)

	def yaw(self,angle, dt):
		self.pitchaxis = GL.UniversalRotationDeg(-angle,self.yawaxis).dot(self.pitchaxis)
		self.rollaxis = GL.UniversalRotationDeg(-angle,self.yawaxis).dot(self.rollaxis)

	def roll(self,angle, dt):
		self.yawaxis = GL.UniversalRotationDeg(-angle,self.rollaxis).dot(self.yawaxis)
		self.pitchaxis = GL.UniversalRotationDeg(-angle,self.rollaxis).dot(self.pitchaxis)

	#This is just basic how to get the rotation matrix between two coordinate frames, because we have to pass that one into the
	#Camera object, we also move from 3 row vectors and math to homogeneous coordinates, because we need that in projecting stuf
	def getrot(self):
		yp = -self.yawaxis
		zp = -self.rollaxis
		xp = self.pitchaxis

		x = np.array([1,0,0])
		y = np.array([0,1,0])
		z = np.array([0,0,1])

		Mat = np.zeros([4,4])
		Mat[3,3] = 1

		Mat[0,0] = np.dot(x,xp)
		Mat[0,1] = np.dot(x,yp)
		Mat[0,2] = np.dot(x,zp)

		Mat[1,0] = np.dot(y,xp)
		Mat[1,1] = np.dot(y,yp)
		Mat[1,2] = np.dot(y,zp)

		Mat[2,0] = np.dot(z,xp)
		Mat[2,1] = np.dot(z,yp)
		Mat[2,2] = np.dot(z,zp)

		#Either Transpose or switch around stuff uptop, but i guessed the right matrix so transpose
		return Mat.transpose()

	#This is for getting the euler angl;es of the airplane
	#The formulas are a little squeezed, but its basically analytical geometry
	#for heading take modulus, because i dunno when do you have the opportunity to use modulus
	#roll somehow was harder
	def geteulers(self):
		pitch = degrees(asin(((((np.dot(self.rollaxis,np.array([0,1,0]))/np.linalg.norm(self.rollaxis)/np.linalg.norm(np.array([0,1,0])))+1)%2)-1)))
		alpha = degrees(asin(((((np.dot(self.v,self.yawaxis)/np.linalg.norm(self.v))+1)%2)-1)))
		heading = degrees(asin(np.dot(self.rollaxis,np.array([1,0,0]))/np.linalg.norm(self.rollaxis)))%360
		#project everything onto the plane perpendicular to the rollaxis, i have to google for that
		projyaw = self.yawaxis-np.dot(self.yawaxis,self.rollaxis)/np.linalg.norm(self.rollaxis)**2 * self.rollaxis
		projy = np.array([0,-1,0])-np.dot(np.array([0,-1,0]),self.rollaxis)/np.linalg.norm(self.rollaxis)**2 * self.rollaxis

		#Call me Daddy befcause that acos function domain error can suck my dick
		roll = degrees(acos((((np.dot(projyaw,projy)/(np.linalg.norm(projy)*np.linalg.norm(projyaw)))+1)%2)-1))
		#The method for deciding if positive or negative, i found on the internet, because im not good at linear algebra
		roll = roll if  np.cross(projyaw,projy)[2]>0 else -roll

		
		#it is important to make this negative positive thing later for the right direction of roll
		#because i dont like cosine, its just a co of the sine and that rightfully
		return pitch, alpha, heading, roll

	#This is to plot groundtrack or i dunno
	def getveleulers(self):
		vert = degrees(asin(np.dot(self.v,np.array([0,1,0]))/np.linalg.norm(self.v)))
		heading = degrees(asin(np.dot(self.v,np.array([1,0,0]))/np.linalg.norm(self.v)))%360
		return vert,heading

	#Just basic physics, Dynamics and Intro to Aerospace 1
	def update(self, dt, braking):
		#sum of Forces around cg
		#all in World Axis system
		self.acc = 9.81 * np.array([0,-1,0])
		Thr = self.Engine.getThrust(dt)
		self.acc += Thr/self.mass * self.rollaxis
		#We actually burn mass, big surprise
		#Not a really beatiful way, but hey
		#Magic numbres are cool, not
		if Thr:
			self.mass -= 106.02*dt

		#Then we makey do Aerodynamics
		p, rho, T = ISA(self.pos[1])
		q = 0.5 * np.linalg.norm(self.v)**2*rho
		#There should be edge case handling for T = 0, but python takes care of that
		Mach = np.linalg.norm(self.v)/sqrt(1.4*287*T)
		self.Mach = Mach
		#This is like 20 lines up, same stuff no magic
		alpha = degrees(asin(np.dot(self.v,self.yawaxis)/np.linalg.norm(self.v)))
		L,D,M = self.Wing.getAerodynamics(q,alpha,Mach)

		#calculate sideslip
		slip = degrees(asin(np.dot(self.v,self.pitchaxis)/np.linalg.norm(self.v)))
		L2,D2,M2 = self.Tail.getAerodynamics(q,slip,Mach)


		#Lift and Drag of all surfaces
		self.acc += -L/self.mass*self.yawaxis
		self.acc += -D/self.mass*self.rollaxis
		self.acc += -L2/self.mass*self.pitchaxis
		self.acc += -D2/self.mass*self.rollaxis
		
		#We call this numerical integration
		self.v += self.acc*dt
		self.pos += self.v*dt

		#for landing
		if self.pos[1] < self.gearlength:
			self.pos[1] = self.gearlength
			self.v[1] = 0

		#make airplane stable without taking moments and stuff
		#This breaks down at anlgews off attack hihger than 90
		self.pitch(-alpha*0.01,1)

		self.yaw(-slip*0.01,1)

		#and we need to brake to come to stop, its not even close to realistic braking
		if braking and self.pos[1] <= self.gearlength:
			self.v /= 1.2


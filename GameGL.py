#pragma once
import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from Flightmodel import X15
from HUD import HUD
import pygame
from math import *
import numpy as np
import sys
from Terrain import Terrain

plane = X15([0,1000,5])
pygame.init()
width = 1000
height = 800
screen = pygame.display.set_mode((width,height),pygame.DOUBLEBUF|pygame.OPENGL)

#For physics
lastt = 0
lastgearswitch = 0

Terr = Terrain(50)

glLoadIdentity()
gluPerspective(45,width/height,0.1,sqrt(2*6371000)*sqrt(abs(plane.pos[1])))
glTranslatef(-plane.pos[0],-plane.pos[1],-plane.pos[2])
glRotatef(0,0,0,0)
while True:
	#More math for physics
	curt = pygame.time.get_ticks()
	keys = pygame.key.get_pressed()
	dt = (curt-lastt)/1000 #makey makey deytey
	plane.update(dt,keys[pygame.K_b])
	#Clear buffer
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

	#This is where i pass my rotated axis plane system into the camera, which we reset with the first line
	target = plane.pos + 4*plane.rollaxis
	glLoadIdentity()
	gluPerspective(30,width/height,0.1,sqrt(2*6371000)*sqrt(abs(plane.pos[1])))
	glTranslatef(-plane.pos[0],-plane.pos[1],-plane.pos[2])
	#OPENGL doesnt seem to take lists, thats why this looks so complex
	gluLookAt(plane.pos[0],plane.pos[1],plane.pos[2],  target[0], target[1], target[2], -plane.yawaxis[0], -plane.yawaxis[1], -plane.yawaxis[2])
	Terr.render()

	glBegin(GL_QUADS)

	glColor3f(1.0, 0.0, 0.0)
	glNormal3f(0.0, 1.0, 0.0)
	glVertex3f(-0.5, 0.5, 0.5)
	glVertex3f(0.5, 0.5, 0.5)
	glVertex3f(0.5, 0.5, -0.5)
	glVertex3f(-0.5, 0.5, -0.5)
 
	glEnd()
 
	glBegin(GL_QUADS)

	glColor3f(0.0, 1.0, 0.0)
	glNormal3f(0.0, 0.0, 1.0)
	glVertex3f(0.5, -0.5, 0.5)
	glVertex3f(0.5, 0.5, 0.5)
	glVertex3f(-0.5, 0.5, 0.5)
	glVertex3f(-0.5, -0.5, 0.5)
 
	glEnd()
 
	glBegin(GL_QUADS)

	glColor3f(0.0, 0.0, 1.0)
	glNormal3f(1.0, 0.0, 0.0)
	glVertex3f(0.5, 0.5, -0.5)
	glVertex3f(0.5, 0.5, 0.5)
	glVertex3f(0.5, -0.5, 0.5)
	glVertex3f(0.5, -0.5, -0.5)
 
	glEnd()
 
	glBegin(GL_QUADS);

	glColor3f(0.0, 0.0, 0.5);
	glNormal3f(-1.0, 0.0, 0.0);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
 
	glEnd()
 
	glBegin(GL_QUADS);

	glColor3f(0.5, 0.0, 0.0)
	glNormal3f(0.0, -1.0, 0.0)
	glVertex3f(0.5, -0.5, 0.5)
	glVertex3f(-0.5, -0.5, 0.5)
	glVertex3f(-0.5, -0.5, -0.5)
	glVertex3f(0.5, -0.5, -0.5)
 
	glEnd()
 
	glBegin(GL_QUADS)

	glColor3f(0.0, 0.5, 0.0);
	glNormal3f(0.0, 0.0, -1.0)
	glVertex3f(0.5, 0.5, -0.5)
	glVertex3f(0.5, -0.5, -0.5)
	glVertex3f(-0.5, -0.5, -0.5)
	glVertex3f(-0.5, 0.5, -0.5)

	glEnd()
 


	#if you remove this you will have to study materials forever
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			sys.exit()

	#Stuff to control
	if keys[pygame.K_w]:
		plane.pitch(0.5,dt)
	if keys[pygame.K_s]:
		plane.pitch(-0.5,dt)

	if keys[pygame.K_a]:
		plane.yaw(0.5,dt)
	if keys[pygame.K_d]:
		plane.yaw(-0.5,dt)

	if keys[pygame.K_q]:
		plane.roll(0.5,dt)
	if keys[pygame.K_e]:
		plane.roll(-0.5,dt)

	if keys[pygame.K_g]:
		#make the gear less flippy
		print((pygame.time.get_ticks()) > lastgearswitch)
		if (pygame.time.get_ticks()) > lastgearswitch:
			plane.switchgear()
			lastgearswitch = pygame.time.get_ticks()

	if keys[pygame.K_SPACE]:
		plane.Engine.ignite()

	curt = lastt
	pygame.display.flip()
	pygame.time.wait(10)
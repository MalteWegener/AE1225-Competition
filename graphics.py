import numpy as np
import Flightmodel
import GL
import pygame
import random

class Camera():
	def __init__(self, pos, rot, aspect):
		self.Translation = GL.TranslateNeg(pos)
		self.Rotation = rot
		self.Projector = GL.Projection(aspect, 60, 0.1, 150000)

	def update(self, pos, rot):
		self.Translation = GL.TranslateNeg(pos)
		self.Rotation = rot

	def apply(self, pt):
		relpos = self.Rotation.dot(self.Translation.dot(pt))
		renderpos = self.Projector.dot(relpos)
		return relpos, renderpos

class GroundTile():
	def __init__(self,pos):
		self.vertices = []
		self.vertices.append(pos+np.array([3,0,3,0]))
		self.vertices.append(pos+np.array([-3,0,3,0]))
		self.vertices.append(pos+np.array([-3,0,-3,0]))
		self.vertices.append(pos+np.array([3,0,-3,0]))
		self.colour = (random.randint(150,220),random.randint(160,180),random.randint(90,130))

	def render(self, screen, cam, width, height):
		renders = []
		for v in self.vertices:
			relpos, renderpos = cam.apply(v)
			if relpos[2] < 0:
				x = (1+renderpos[0])*width/2
				y = (1-renderpos[1])*height/2
				if -100<x<width+100 and -100<y<height+100:
					renders.append((x,y))
		if len(renders) == 4:
			print(renders[0])
			pygame.draw.polygon(screen,self.colour,renders)


class Ground():
	def __init__(self):
		self.grounds = []
		for i in range(-500,500,6):
			for j in range(-500,500,6):
				self.grounds.append(GroundTile(np.array([i,0,j,1])))

class Game():
	def __init__(self, width, height):
		pygame.init()
		self.width = width
		self.height = height
		self.Ground = Ground()
		self.screen = pygame.display.set_mode((width,height))
		self.airplane = Flightmodel.X15(np.array([0,100,0]))
		self.Camera = Camera(self.airplane.pos,self.airplane.getrot(),width/height)

	def render(self):
		for g in self.Ground.grounds:
			g.render(self.screen,self.Camera,self.width,self.height)
		pygame.display.update()

Game = Game(800,600)
while True:
	Game.render()

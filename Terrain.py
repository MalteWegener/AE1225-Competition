#pragma once
import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
import numpy as np
import sys
import random

class Terrain():
	def __init__(self, res):
		self.heightmap = np.zeros([100,100])
		self.res = res

		for i in range(self.heightmap.shape[0]):
			for j in range(self.heightmap.shape[1]):
				self.heightmap[i,j] = random.random()

	def render(self):
		res = self.res
		#Make terrain with triangulation
		for i in range(1,self.heightmap.shape[0]-1,2):
			for j in range(1,self.heightmap.shape[0]-1,2):
				x = res*(i-self.heightmap.shape[0]/2)
				z = res*(j-self.heightmap.shape[1]/2)

				#Make 4 quads
				glBegin(GL_QUADS)
				col = self.heightmap[i,j]
				glColor3f(col, 0,0)
				glVertex3f(x, self.heightmap[i,j], z)

				col = self.heightmap[i-1,j]
				glColor3f(col, 0,0)
				glVertex3f(x-res, self.heightmap[i-1,j], z)

				col = self.heightmap[i-1,j-1]
				glColor3f(col, 0,0)
				glVertex3f(x-res, self.heightmap[i-1,j-1], z-res)

				col = self.heightmap[i,j-1]
				glColor3f(col, 0,0)
				glVertex3f(x, self.heightmap[i,j-1], z-res)

				glEnd()

				glBegin(GL_QUADS)
				col = self.heightmap[i,j]
				glColor3f(col,0,0)
				glVertex3f(x, self.heightmap[i,j], z)

				col = self.heightmap[i+1,j]
				glColor3f(col, 0,0)
				glVertex3f(x+res, self.heightmap[i+1,j], z)

				col = self.heightmap[i+1,j-1]
				glColor3f(col, 0,0)
				glVertex3f(x+res, self.heightmap[i+1,j-1], z-res)

				col = self.heightmap[i,j-1]
				glColor3f(col, 0,0)
				glVertex3f(x, self.heightmap[i,j-1], z-res)

				glEnd()

				glBegin(GL_QUADS)
				col = self.heightmap[i,j]
				glColor3f(col, 0,0)
				glVertex3f(x, self.heightmap[i,j], z)

				col = self.heightmap[i+1,j]
				glColor3f(col, 0,0)
				glVertex3f(x+res, self.heightmap[i+1,j], z)

				col = self.heightmap[i+1,j+1]
				glColor3f(col, 0,0)
				glVertex3f(x+res, self.heightmap[i+1,j+1], z+res)

				col = self.heightmap[i,j+1]
				glColor3f(col, 0,0)
				glVertex3f(x, self.heightmap[i,j+1], z+res)

				glEnd()


				glBegin(GL_QUADS)
				col = self.heightmap[i,j]
				glColor3f(col, 0,0)
				glVertex3f(x, self.heightmap[i,j], z)

				col = self.heightmap[i-1,j]
				glColor3f(col, 0,0)
				glVertex3f(x-res, self.heightmap[i-1,j], z)

				col = self.heightmap[i-1,j+1]
				glColor3f(col, 0,0)
				glVertex3f(x-res, self.heightmap[i-1,j+1], z+res)

				col = self.heightmap[i,j+1]
				glColor3f(col, 0,0)
				glVertex3f(x, self.heightmap[i,j+1], z+res)

				glEnd()